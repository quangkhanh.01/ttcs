package com.example.TT.run;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class AHController {
	@GetMapping("/ahs")
		public String getAHs(Model model) throws IOException {
			Connection connection = null;
			Statement statement = null;
			ResultSet resultSet = null;
			ArrayList<AH> ahs = new ArrayList<AH>();
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/anhhung", "root", "X.sS!rHW^4!TMd9");
				statement = connection.createStatement();
				resultSet = statement.executeQuery("select * from anhhung;");
				while (resultSet.next()) {
					int maqn = resultSet.getInt("MaQN");
					String ten = resultSet.getString("Ten");
					String sinh = resultSet.getString("Sinh");
					String mat = resultSet.getString("Mat");
					String nuoc = resultSet.getString("NuocCH");
					String cb = resultSet.getString("CapBac");
					String thuong = resultSet.getString("Thuong");
					ahs.add(new AH(maqn, ten, sinh, mat, nuoc, cb, thuong));
				}
			}
			catch (Exception e) {
				e.printStackTrace();	
			}
			model.addAttribute("ahs", ahs);
			return "ahs";
	}
	@GetMapping("/ah/{maqn}")
	public String getAH(Model model, @PathVariable String maqn) {
		model.addAttribute("maqn", maqn);
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet result = null;

		AH ah = new AH();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/anhhung", "root", "X.sS!rHW^4!TMd9");
			ps = connection.prepareStatement("select * from anhhung where maqn = ?;");
			ps.setInt(1, Integer.valueOf(maqn));
			result = ps.executeQuery();
			while (result.next()) {
				ah.setMaqn(result.getInt("MaQN"));
				ah.setTen(result.getString("Ten"));
				ah.setSinh(result.getString("Sinh"));
				ah.setMat(result.getString("Mat"));
				ah.setNuoc(result.getString("NuocCH"));
				ah.setCb(result.getString("CapBac"));
				ah.setThuong(result.getString("Thuong"));
			}
		} // End of try block
		catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("ah", ah);
		return "ah-detail";
	}

}
