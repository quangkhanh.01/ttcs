package com.example.TT.run;

public class AH {
	private int maqn;
	private String ten;
	private String sinh;
	private String mat;
	private String nuoc;
	private String cb;
	private String thuong;
	
	
	public AH() {
		super();
	}
	
	public AH(int maqn, String ten, String sinh, String mat, String nuoc, String cb, String thuong) {
		super();
		this.maqn = maqn;
		this.ten = ten;
		this.sinh = sinh;
		this.mat = mat;
		this.nuoc = nuoc;
		this.cb = cb;
		this.thuong = thuong;
	}

	public int getMaqn() {
		return maqn;
	}

	public void setMaqn(int maqn) {
		this.maqn = maqn;
	}

	public String getTen() {
		return ten;
	}

	public void setTen(String ten) {
		this.ten = ten;
	}

	public String getSinh() {
		return sinh;
	}

	public void setSinh(String sinh) {
		this.sinh = sinh;
	}

	public String getMat() {
		return mat;
	}

	public void setMat(String mat) {
		this.mat = mat;
	}

	public String getNuoc() {
		return nuoc;
	}

	public void setNuoc(String nuoc) {
		this.nuoc = nuoc;
	}

	public String getCb() {
		return cb;
	}

	public void setCb(String cb) {
		this.cb = cb;
	}

	public String getThuong() {
		return thuong;
	}

	public void setThuong(String thuong) {
		this.thuong = thuong;
	}

	
}
