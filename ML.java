package com.example.TT.run;

public class ML {
	private int tt;
	private int malenh;
	private String tieude;
	private String noidung;
	
	public ML() {
		super();
	}
	
	public ML(int tt, int malenh, String tieude, String noidung) {
		super();
		this.tt = tt;
		this.malenh = malenh;
		this.tieude = tieude;
		this.noidung = noidung;
	}

	public int getTt() {
		return tt;
	}

	public void setTt(int tt) {
		this.tt = tt;
	}

	public int getMalenh() {
		return malenh;
	}

	public void setMalenh(int malenh) {
		this.malenh = malenh;
	}

	public String getTieude() {
		return tieude;
	}

	public void setTieude(String tieude) {
		this.tieude = tieude;
	}

	public String getNoidung() {
		return noidung;
	}

	public void setNoidung(String noidung) {
		this.noidung = noidung;
	}
	
	
}
