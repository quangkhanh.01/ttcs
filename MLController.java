package com.example.TT.run;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class MLController {
	@GetMapping("/mls")
		public String getMLs(Model model) throws IOException {
			Connection connection = null;
			Statement statement = null;
			ResultSet resultSet = null;
			ArrayList<ML> mls = new ArrayList<ML>();
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/menhlenh", "root", "X.sS!rHW^4!TMd9");
				statement = connection.createStatement();
				resultSet = statement.executeQuery("select * from menhlenh");
				while (resultSet.next()) {
					int tt = resultSet.getInt("TT");
					int malenh = resultSet.getInt("MaLenh");
					String tieude = resultSet.getString("TieuDe");
					String noidung = resultSet.getString("NoiDung");
					mls.add(new ML(tt, malenh, tieude, noidung));
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			model.addAttribute("mls", mls);
			return "mls";
	}
	@GetMapping("/ml/{tt}")
	public String getML(Model model, @PathVariable String tt) {
		model.addAttribute("tt", tt);
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet result = null;

		ML ml = new ML();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/menhlenh", "root", "X.sS!rHW^4!TMd9");
			ps = connection.prepareStatement("select * from menhlenh where tt = ?");
			ps.setInt(1, Integer.valueOf(tt));
			result = ps.executeQuery();
			while (result.next()) {
				ml.setTt(result.getInt("TT"));
				ml.setMalenh(result.getInt("MaLenh"));
				ml.setTieude(result.getString("TieuDe"));
				ml.setNoidung(result.getString("NoiDung"));
			}
		} // End of try block
		catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("ml", ml);
		return "ml-detail";
	}
}
